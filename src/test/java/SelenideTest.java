import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class SelenideTest {
    @Test
    public void selenideTest1(){
        open("https://kharkiv.ithillel.ua/");
        $(By.xpath("/html/body/section[1]/div/article/div/div/figure/img")).exists();

    }
    @Test
    public void selenideTest2(){
        open("https://kharkiv.ithillel.ua/");
        $(By.xpath("//*[@id=\"signCoursesButton\"]")).click();
        $(By.xpath("//*[@id=\"signCourses\"]/div/form/div/div[2]")).should(Condition.appear);
    }
    @Test
    public void selenideTest3(){
        open("https://kharkiv.ithillel.ua/");
        $(By.xpath("//*[@id=\"school\"]")).click();
        $(By.xpath("//*[@id=\"school-menu\"]/div")).shouldBe(Condition.visible);
    }
    @Test
    public void selenideTest4(){
        open("https://kharkiv.ithillel.ua/");
        $(By.xpath("//*[@id=\"school\"]")).click();
        $(By.xpath("//*[@id=\"school\"]")).click();
        $(By.xpath("//*[@id=\"school-menu\"]/div")).shouldBe(Condition.disappear);
    }
    @Test
    public void selenideTest5(){
        open("https://kharkiv.ithillel.ua/");
        $(By.xpath("//*[@id=\"course\"]")).click();
        $(By.xpath("//*[@id=\"searchInput\"]")).click();
        $(By.xpath("//*[@id=\"searchInput\"]")).setValue("qwe");
        $(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[3]/div[2]/div[2]/span[3]")).should(Condition.appear);
        $(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[3]/div[2]/div[2]/span[2]")).shouldBe(Condition.disappear);
    }

}